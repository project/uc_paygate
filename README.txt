
Paygate is a South African payment gateway for the Ubercart ecommerce module for Drupal

www.paygate.co.za

Developed by Quentin Campbell (Max_Headroom) www.maxheadroom.co.za


Installation:
No special instructions to install. Upload and enable module as normal.

Configure:
In admin/store/settings/payment enable Paygate as a payment method. 
Under admin/store/settings/payment/edit/methods, click on Paygate settings.
Enter the Paygate ID and Checksome key you were given by Paygate
Leave Test Paygate ID and Test Checksum key as is.
Select transaction mode to be Test or Production (defaults to Test). Use test to check your installation. Testing instructions below.
Change return URL to suit your needs. The default is enough to show transaction messages and new user register, etc.
Change titles and instructions to suit your layout.

Testing instructions:

Information below from: http://www.paygate.co.za/download/PayWebv2_v1.10.zip

You can complete a test purchase with Paygate using the numbers below.

Paygate use the following payment methods:

    * Credit Card
    * FNB Cell Pay Point (Payment by FNB cellphone banking)
    * Mobux (Debit card type payment) 

When asked for credit card information, you can make up your own information. Just make sure that the expiry date is in the future.

Approved Transactions
Visa 	              4000000000000002
MasterCard 	        5200000000000015
American Express 	  378282246310005
FNB Cell Pay Point 	Enter: "MR PASS"
Mobux 	            6397729999003950

You can also test for failures:

Insufficient funds
Visa 	              4000000000000028
MasterCard 	        5200000000000023
American Express 	  371449635398431

Declined transactions
Visa 	              4000000000000036
MasterCard 	        5200000000000049
American Express 	  30569309025904
FNB Cell Pay Point 	Enter: "MR FAIL"
Mobux 	            6397729999003550

Invalid Card Number
All other card numbers (in test mode).

Unprocessed Transactions
MasterCard 	        5200000000000064

readme.txt v0.03
